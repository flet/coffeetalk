const db = require('./db')
const Ajv = require('ajv')

function create (user, topicData) {
  if (!user) throw new Error('no user')

  var valid = validate(topicData)
  if (!valid.isValid) throw new Error(JSON.stringify(valid.errors))

  return db.topic.create({
    title: topicData.title,
    audience: topicData.audience,
    description: topicData.description,
    status: 'draft',
    userId: user.id
  })
}

function update (existingTopic, topicData) {
  var valid = validate(topicData)
  if (!valid.isValid) throw new Error(JSON.stringify(valid.errors))

  existingTopic.update({
    title: topicData.title,
    audience: topicData.audience,
    description: topicData.description
  })

  return existingTopic.save()
}

function validate (data) {
  var ajv = new Ajv()
  var valid = ajv.validate(topicDataSchema, data)

  return {
    isValid: valid,
    errors: ajv.errors
  }
}

async function updateStatus (hash, newStatus) {
  if (db.topic.rawAttributes.status.values.indexOf(newStatus) < 0) throw new Error('bad status')

  var topic = await db.topic.findOne({ where: { hash: hash } })

  if (!topic) throw new Error('bad topic')

  return topic.update({
    status: newStatus
  })
}

function fetch (hash) {
  return db.topic.findOne({ where: { hash: hash } })
}

function buildAudiences (selectedValue) {
  if (!selectedValue) selectedValue = db.topic.rawAttributes.audience.values[0]

  return db.topic.rawAttributes.audience.values.map(v => { return { name: v, selected: v === selectedValue } })
}

function fetchOpenTopics () {
  return db.topic.findAll({ where: { status: 'published' } })
}

module.exports = {
  fetch,
  update,
  create,
  buildAudiences,
  updateStatus,
  fetchOpenTopics
}

var topicDataSchema = {
  properties: {
    title: { type: 'string' },
    audience: { type: 'string', enum: db.topic.rawAttributes.audience.values },
    description: { type: 'string' }
  },
  required: ['title', 'audience', 'description']
}
