
module.exports = function (sequelize, DataTypes) {
  var user = sequelize.define('user', {
    firstName: {
      type: DataTypes.STRING
    },
    lastName: {
      type: DataTypes.STRING
    },
    username: {
      type: DataTypes.STRING
    },
    password: {
      type: DataTypes.STRING
    }
  })

  user.associate = function (models) {
    // associations can be defined here
    user.hasMany(models.topic, { as: 'Topics' })
  }

  return user
}
