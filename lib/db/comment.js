var uid = require('../uid')

module.exports = function (sequelize, DataTypes) {
  var comment = sequelize.define('comment', {
    comment: {
      type: DataTypes.TEXT
    },
    hash: {
      type: DataTypes.STRING,
      unique: true,
      defaultValue: function () {
        return uid.generate()
      }
    },
    thumbsup: {
      type: DataTypes.INTEGER,
      defaultValue: 0
    },
    thumbsdown: {
      type: DataTypes.INTEGER,
      defaultValue: 0
    },
    anonHash: {
      type: DataTypes.STRING
    }
  })

  comment.associate = function (models) {
    // associations can be defined here
    comment.belongsTo(models.user)
    comment.belongsTo(models.topic)
  }

  return comment
}
