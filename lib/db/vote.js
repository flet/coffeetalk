module.exports = function (sequelize, DataTypes) {
  var vote = sequelize.define('vote', {
    thumbs: {
      type: DataTypes.ENUM('up', 'down')
    },
    anonHash: {
      type: DataTypes.STRING
    }
  })

  vote.associate = function (models) {
    // associations can be defined here
    vote.belongsTo(models.user)
    vote.belongsTo(models.comment)
  }

  return vote
}
