var express = require('express')
var router = express.Router()

/* GET home page. */
router.get('/', async function (req, res, next) {
  if (process.env.INTRO_PARAGRAPH) res.locals.intro_paragraph = process.env.INTRO_PARAGRAPH
  res.locals.title = `Welcome to ${res.locals.app_name}`
  const locals = { active: { homeNav: true } }

  if (!req.user) return res.render('index', locals)

  locals.userTopics = await req.user.getTopics()
  res.render('index', locals)
})

module.exports = router
