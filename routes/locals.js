var uid = require('../lib/uid')

module.exports = function () {
  return function (req, res, next) {
    res.locals.app_name = process.env.APP_NAME || 'Coffee Talk'
    res.locals.user = req.user

    // setup anonymous ID
    if (!req.session.anonHash) {
      req.session.anonHash = uid.generate()
    }
    res.locals.anonHash = req.session.anonHash

    next()
  }
}
