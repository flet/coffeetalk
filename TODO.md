# Things to do

- topics should have a "close time" which locks the topic and disallows further votes/comments
- topics should have an "open time" which makes the topic available for vote
- topic authors should be able to push a "close now" button to force a topic closed
- add new user sign up
- closed topics should be shown in their own table under /topics
- associate a top-level vote with the topic itself (like a 1-5 rating)
- add "sub comments" (no voting?)
- handle thrown errors a bit nicer for validation errors

# Future ideas
- comment moderation?
- email notification of comments/topics
- custom slugs?
- use third party for user auth (avoid storing user/pw)?
